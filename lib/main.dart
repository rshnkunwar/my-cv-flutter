import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:url_launcher/url_launcher.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "My CV App",
      theme: ThemeData(
        primarySwatch: Colors.deepOrange,
      ),
      home: Scaffold(
        appBar: AppBar(
          leading: Icon(Icons.laptop_mac),
          centerTitle: true,
          title: Text("My CV App"),
        ),
        body: SingleChildScrollView(
            child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Card(
                child: Padding(
                  padding: EdgeInsets.symmetric(
                    vertical: 10.0,
                    horizontal: 10.0
                    ),
                  // margin: EdgeInsets.only(bottom: 20.0),
                  child: Column(
                    children: <Widget>[
                      CircleAvatar(
                        backgroundColor: Colors.orange,
                        radius: 50.00,
                        backgroundImage: AssetImage("assets/probability.jpg"),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text("Roshan Jung Kunwar",
                       style: TextStyle(
                         fontSize: 25.0
                      ),
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      Text("App Developer",
                        style: TextStyle(
                          color: Colors.blueGrey,
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold
                        ),
                      ),
                      SizedBox(
                        height: 08.0,
                      ),
                      Text(
                          "Hi, I am Roshan Jung. I am a web plus app developer. I am also a software engineer. Aspire to be a great developer.",
                          style: TextStyle(
                            fontSize: 14.0,
                            fontStyle: FontStyle.normal
                          ),
                          ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                  left: 10.0,
                  top: 10.0,
                  bottom: 20.0
                ),
                child: Center(
                  child: Text("SOCIAL LINKS",
                   style: TextStyle(
                     fontSize: 20.0,
                     
                   )),
                ),
              ),

              Card(
                  elevation: 5.0,
                  child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    IconButton(
                      onPressed: () {
                        launch("https://www.facebook.com/rshnkunwar");
                      },
                      color: Colors.blue,
                      icon: Icon(FontAwesomeIcons.facebookF),
                    ),
                    IconButton(
                      onPressed: () {
                        launch("https://www.twitter.com/rshnkunwar");
                      },
                      color: Colors.blue,
                      icon: Icon(FontAwesomeIcons.twitter),
                    ),
                    IconButton(
                      onPressed: () {
                        launch("https://www.reddit.com/rshnkunwar");
                      },
                      color: Colors.red,
                      icon: Icon(FontAwesomeIcons.reddit),
                    ),
                    IconButton(
                      onPressed: () {
                        launch("https://www.linkedin.com/rshnkunwar");
                      },
                      color: Colors.blue,
                      icon: Icon(FontAwesomeIcons.linkedin),
                    ),
                    IconButton(
                      onPressed: () {
                        launch("https://www.instagram.com/rshnkunwar");
                      },
                      color: Colors.red,
                      icon: Icon(FontAwesomeIcons.instagram),
                    )
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                  left: 10.0,
                  top: 10.0,
                  bottom: 20.0
                ),
                child: Center(
                  child: Text("Skills",
                   style: TextStyle(
                     fontSize: 20.0,
                   )),
                ),
              ),

              Card(
                elevation: 2.0,
                child: ListTile(
                  leading: CircleAvatar(
                    radius: 25.0,
                    backgroundImage: NetworkImage("https://i.udemycdn.com/course/240x135/548278_b005_9.jpg"),
                  ),
                  title: Text("Web Development"),
                  subtitle: Text("Laravel, BootStrap,API Developer"),
                ),
              ),
              SizedBox(
                height: 15.0,
              ),
              Card(
                elevation: 2.0,
                child: ListTile(
                  leading: CircleAvatar(
                    radius: 25.0,
                    backgroundImage: NetworkImage("https://cdn-images-1.medium.com/max/1600/1*2NhFI88b90GxRjtaaW7Epw.png"),
                  ),
                  title: Text("App Development"),
                  subtitle: Text("Dart Flutter"),
                ),
              ),
              SizedBox(
                height: 20.0,
              ),

            ],
          ),
        ),
      ),
    );
  }
}
